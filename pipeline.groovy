pipeline {
  agent any
  triggers{
    githubPush()
  }
  stages {
    stage('build'){
      steps {
        sh 'docker build -t 4248-ishan-app .'
      }
    }
    stage('run'){
      steps{
        sh 'docker run -d -p 5000:3000 4248-ishan-app'
      }
    }
    stage('final'){
      steps{
        sh 'docker ps'
      }
    }
  }
}